package xyz.maziki.malawiwatch;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.wearable.complications.ComplicationHelperActivity;
import android.support.wearable.complications.ComplicationProviderInfo;
import android.support.wearable.complications.ProviderChooserIntent;
import android.support.wearable.complications.ProviderInfoRetriever;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.concurrent.Executors;

/**
 * The watch-side config activity for {@link WatchfaceService}, which allows for setting
 * the left, right, top and bottom complications of watch face.
 */
public class ComplicationConfigActivity extends Activity implements View.OnClickListener {

    static final int COMPLICATION_CONFIG_REQUEST_CODE = 1001;
    private static final String TAG = "ConfigActivity";

    // Selected complication id by user.
    private int mSelectedComplicationId;
    // ComponentName used to identify a specific service that renders the watch face.
    private ComponentName mWatchFaceComponentName;
    // Required to retrieve complication data from watch face for preview.
    private ProviderInfoRetriever mProviderInfoRetriever;
    private ImageView mLeftComplicationBackground;
    private ImageView mRightComplicationBackground;
    private ImageButton mLeftComplication;
    private ImageButton mRightComplication;
    private Drawable mDefaultAddComplicationDrawable;
    private ImageView mBottomComplicationBackground;
    private ImageButton mBottomComplication;
    private ImageView mTopComplicationBackground;
    private ImageButton mTopComplication;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_config);

        mDefaultAddComplicationDrawable = getDrawable(R.drawable.add_complication);

        mSelectedComplicationId = -1;

        mWatchFaceComponentName =
                new ComponentName(getApplicationContext(), WatchfaceService.class);


        // Sets up left complication preview.
        mLeftComplicationBackground = findViewById(R.id.left_complication_background);
        mLeftComplication = findViewById(R.id.left_complication);
        mLeftComplication.setOnClickListener(this);

        // Sets default as "Add Complication" icon.
        mLeftComplication.setImageDrawable(mDefaultAddComplicationDrawable);
        mLeftComplicationBackground.setVisibility(View.INVISIBLE);

        // Sets up right complication preview.
        mRightComplicationBackground = findViewById(R.id.right_complication_background);
        mRightComplication = findViewById(R.id.right_complication);
        mRightComplication.setOnClickListener(this);

        // Sets default as "Add Complication" icon.
        mRightComplication.setImageDrawable(mDefaultAddComplicationDrawable);
        mRightComplicationBackground.setVisibility(View.INVISIBLE);

        // Sets up bottom complication preview.
        mBottomComplicationBackground = findViewById(R.id.bottom_complication_background);
        mBottomComplication = findViewById(R.id.bottom_complication);
        mBottomComplication.setOnClickListener(this);

        // Sets default as "Add Complication" icon.
        mBottomComplication.setImageDrawable(mDefaultAddComplicationDrawable);
        mBottomComplicationBackground.setVisibility(View.INVISIBLE);

        // Sets up Top complication preview.
        mTopComplicationBackground = findViewById(R.id.top_complication_background);
        mTopComplication = findViewById(R.id.top_complication);
        mTopComplication.setOnClickListener(this);

        // Sets default as "Add Complication" icon.
        mTopComplication.setImageDrawable(mDefaultAddComplicationDrawable);
        mTopComplicationBackground.setVisibility(View.INVISIBLE);

        // initialize 2
        mProviderInfoRetriever =
                new ProviderInfoRetriever(getApplicationContext(), Executors.newCachedThreadPool());
        mProviderInfoRetriever.init();

        retrieveInitialComplicationsData();

    }


    // retrieve complication data
    public void retrieveInitialComplicationsData() {

        final int[] complicationIds = WatchfaceService.getComplicationIds();

        mProviderInfoRetriever.retrieveProviderInfo(
                new ProviderInfoRetriever.OnProviderInfoReceivedCallback() {
                    @Override
                    public void onProviderInfoReceived(
                            int watchFaceComplicationId,
                            @Nullable ComplicationProviderInfo complicationProviderInfo) {

                        Log.d(TAG, "\n\nonProviderInfoReceived: " + complicationProviderInfo);

                        updateComplicationViews(watchFaceComplicationId, complicationProviderInfo);
                    }
                },
                mWatchFaceComponentName,
                complicationIds);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //release
        mProviderInfoRetriever.release();
    }

    // Verifies the watch face supports the complication location, then launches the helper
    // class, so user can choose their complication data provider.
    // launch data selector
    private void launchComplicationHelperActivity(ComplicationLocation complicationLocation) {

        mSelectedComplicationId = WatchfaceService.getComplicationId(complicationLocation);

        if (mSelectedComplicationId >= 0) {

            int[] supportedTypes =
                    WatchfaceService.getSupportedComplicationTypes(
                            complicationLocation);

            startActivityForResult(
                    ComplicationHelperActivity.createProviderChooserHelperIntent(
                            getApplicationContext(),
                            mWatchFaceComponentName,
                            mSelectedComplicationId,
                            supportedTypes),
                    ComplicationConfigActivity.COMPLICATION_CONFIG_REQUEST_CODE);

        } else {
            Log.d(TAG, "Complication not supported by watch face.");
        }
    }

    @Override
    public void onClick(View view) {
        if (view.equals(mLeftComplication)) {
            Log.d(TAG, "Left Complication click()");
            launchComplicationHelperActivity(ComplicationLocation.LEFT);

        } else if (view.equals(mRightComplication)) {
            Log.d(TAG, "Right Complication click()");
            launchComplicationHelperActivity(ComplicationLocation.RIGHT);

        } else if (view.equals(mBottomComplication)) {
            Log.d(TAG, "Bottom Complication click()");
            launchComplicationHelperActivity(ComplicationLocation.BOTTOM);

        } else if (view.equals(mTopComplication)) {
            Log.d(TAG, "Top Complication click()");
            launchComplicationHelperActivity(ComplicationLocation.TOP);

        }
    }

    public void updateComplicationViews(
            int watchFaceComplicationId, ComplicationProviderInfo complicationProviderInfo) {
        Log.d(TAG, "updateComplicationViews(): id: " + watchFaceComplicationId);
        Log.d(TAG, "\tinfo: " + complicationProviderInfo);


        switch (watchFaceComplicationId) {
            case WatchfaceService.LEFT_COMPLICATION_ID:
                if (complicationProviderInfo != null) {
                    mLeftComplication.setImageIcon(complicationProviderInfo.providerIcon);
                    mLeftComplicationBackground.setVisibility(View.VISIBLE);

                } else {
                    mLeftComplication.setImageDrawable(mDefaultAddComplicationDrawable);
                    mLeftComplicationBackground.setVisibility(View.INVISIBLE);
                }
                break;

            case WatchfaceService.RIGHT_COMPLICATION_ID:
                if (complicationProviderInfo != null) {
                    mRightComplication.setImageIcon(complicationProviderInfo.providerIcon);
                    mRightComplicationBackground.setVisibility(View.VISIBLE);

                } else {
                    mRightComplication.setImageDrawable(mDefaultAddComplicationDrawable);
                    mRightComplicationBackground.setVisibility(View.INVISIBLE);
                }
                break;

            case WatchfaceService.BOTTOM_COMPLICATION_ID:
                if (complicationProviderInfo != null) {
                    mBottomComplication.setImageIcon(complicationProviderInfo.providerIcon);
                    mBottomComplicationBackground.setVisibility(View.VISIBLE);

                } else {
                    mBottomComplication.setImageDrawable(mDefaultAddComplicationDrawable);
                    mBottomComplicationBackground.setVisibility(View.INVISIBLE);

                }
                break;

            case WatchfaceService.TOP_COMPLICATION_ID:
                if (complicationProviderInfo != null) {
                    mTopComplication.setImageIcon(complicationProviderInfo.providerIcon);
                    mTopComplicationBackground.setVisibility(View.VISIBLE);

                } else {
                    mTopComplication.setImageDrawable(mDefaultAddComplicationDrawable);
                    mTopComplicationBackground.setVisibility(View.INVISIBLE);

                }
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //update views
        if (requestCode == COMPLICATION_CONFIG_REQUEST_CODE && resultCode == RESULT_OK) {

            // Retrieves information for selected Complication provider.
            ComplicationProviderInfo complicationProviderInfo =
                    data.getParcelableExtra(ProviderChooserIntent.EXTRA_PROVIDER_INFO);
            Log.d(TAG, "Provider: " + complicationProviderInfo);

            if (mSelectedComplicationId >= 0) {
                updateComplicationViews(mSelectedComplicationId, complicationProviderInfo);
            }
        }
    }

    /**
     * Used by associated watch face ({@link WatchfaceService}) to let this
     * configuration Activity know which complication locations are supported, their ids, and
     * supported complication data types.
     */
    //intro enum
    public enum ComplicationLocation {
        LEFT,
        RIGHT,
        BOTTOM,
        TOP
    }

}
