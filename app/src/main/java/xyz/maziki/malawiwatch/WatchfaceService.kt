package xyz.maziki.malawiwatch

import android.app.PendingIntent
import android.content.*
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.wearable.complications.ComplicationData
import android.support.wearable.complications.ComplicationHelperActivity
import android.support.wearable.complications.rendering.ComplicationDrawable
import android.support.wearable.watchface.CanvasWatchFaceService
import android.support.wearable.watchface.WatchFaceService
import android.support.wearable.watchface.WatchFaceStyle
import android.util.Log
import android.util.SparseArray
import android.view.SurfaceHolder
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Updates rate in milliseconds for interactive mode. We update once a second to advance the
 * second hand.
 */
private const val INTERACTIVE_UPDATE_RATE_MS = 1000

/**
 * Handler message id for updating the time periodically in interactive mode.
 */
internal const val MSG_UPDATE_TIME = 0

private const val HOUR_STROKE_WIDTH = 5f
private const val MINUTE_STROKE_WIDTH = 3f
internal const val SECOND_TICK_STROKE_WIDTH = 2f

internal const val CENTER_GAP_AND_CIRCLE_RADIUS = 4f

internal const val SHADOW_RADIUS = 6f


class WatchfaceService : CanvasWatchFaceService() {
    override fun onCreateEngine(): Engine {
        return Engine()
    }

    inner class Engine : CanvasWatchFaceService.Engine() {

        private var mCalendar: Calendar? = null
        private var mRegisteredTimeZoneReceiver = false

        private var mCenterX: Float = 0.toFloat()
        private var mCenterY: Float = 0.toFloat()

        private var mHourHandLength: Float = 0.toFloat()
        private var mMinuteHandLength: Float = 0.toFloat()
        private var mSecondHandLength: Float = 0.toFloat()

        private var mHourMinuteTicksHandPaint: Paint = Paint()
        private var mSecondHandPaint: Paint = Paint()

        private var mBackgroundDrawable: Drawable? = null

        private var mAmbient: Boolean = false

        /*
         * Whether the display supports fewer bits for each color in ambient mode.
         * When true, we disable anti-aliasing in ambient mode.
         */
        private var mLowBitAmbient: Boolean = false

        /*
         * Whether the display supports burn in protection in ambient mode.
         * When true, remove the background in ambient mode.
         */
        private var mBurnInProtection: Boolean = false


        // intro 2
        /* Maps active complication ids to the data for that complication. Note: Data will only be
         * present if the user has chosen a provider via the settings activity for the watch face.
         */
        private var mActiveComplicationDataSparseArray: SparseArray<ComplicationData>? = null

        /* Maps complication ids to corresponding ComplicationDrawable that renders the
         * the complication data on the watch face.
         */
        private var mComplicationDrawableSparseArray: SparseArray<ComplicationDrawable>? = null

        private val mTimeZoneReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                mCalendar?.timeZone = TimeZone.getDefault()
                invalidate()
            }
        }

        // Handler to update the time once a second in interactive mode.
        private val mUpdateTimeHandler = object : Handler() {
            override fun handleMessage(message: Message) {

                invalidate()
                if (shouldTimerBeRunning()) {
                    val timeMs = System.currentTimeMillis()
                    val delayMs = INTERACTIVE_UPDATE_RATE_MS - timeMs % INTERACTIVE_UPDATE_RATE_MS
                    this.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs)
                }
            }
        }

        override fun onCreate(holder: SurfaceHolder) {
            super.onCreate(holder)

            setWatchFaceStyle(
                    WatchFaceStyle.Builder(this@WatchfaceService)
                            .setAcceptsTapEvents(true)
                            .build())

            mCalendar = Calendar.getInstance()

            initializeBackground()

            initializeComplications()

            initializeHands()
        }


        private fun initializeBackground() {
            mBackgroundDrawable = applicationContext.getDrawable(R.drawable.background_watchface)
        }

        //initializeComplications()
        private fun initializeComplications() {
            Log.d(TAG, "initializeComplications()")

            mActiveComplicationDataSparseArray = SparseArray(complicationIds.size)

            val leftComplicationDrawable = getDrawable(R.drawable.custom_complication_styles) as ComplicationDrawable
            leftComplicationDrawable.setContext(applicationContext)

            val rightComplicationDrawable = getDrawable(R.drawable.custom_complication_styles) as ComplicationDrawable
            rightComplicationDrawable.setContext(applicationContext)

            val bottomComplicationDrawable = getDrawable(R.drawable.custom_complication_styles) as ComplicationDrawable
            bottomComplicationDrawable.setContext(applicationContext)


            val topComplicationDrawable = getDrawable(R.drawable.custom_complication_styles) as ComplicationDrawable
            topComplicationDrawable.setContext(applicationContext)

            mComplicationDrawableSparseArray = SparseArray(complicationIds.size)
            mComplicationDrawableSparseArray?.apply {
                put(LEFT_COMPLICATION_ID, leftComplicationDrawable)
                put(RIGHT_COMPLICATION_ID, rightComplicationDrawable)
                put(BOTTOM_COMPLICATION_ID, bottomComplicationDrawable)
                put(TOP_COMPLICATION_ID, topComplicationDrawable)
            }

            setActiveComplications(*complicationIds)
            //tells Android Wear that your watch face supports complications, and it requires that you
            // pass the unique IDs for each complication. (These are the same constants we defined at the beginning of the lesson.)
        }

        private fun initializeHands() {
            mHourMinuteTicksHandPaint.apply {
                color = Color.WHITE
                strokeWidth = HOUR_AND_MINUTE_STROKE_WIDTH
                isAntiAlias = true
                strokeCap = Paint.Cap.ROUND
                setShadowLayer(SHADOW_RADIUS.toFloat(), 0f, 0f, Color.BLACK)
            }

            mSecondHandPaint.apply {
                color = Color.RED
                strokeWidth = SECOND_TICK_STROKE_WIDTH
                isAntiAlias = true
                strokeCap = Paint.Cap.ROUND
                setShadowLayer(SHADOW_RADIUS.toFloat(), 0f, 0f, Color.BLACK)
            }

        }

        override fun onDestroy() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME)
            super.onDestroy()
        }

        override fun onPropertiesChanged(properties: Bundle?) {
            mLowBitAmbient = properties!!.getBoolean(WatchFaceService.PROPERTY_LOW_BIT_AMBIENT, false)
            mBurnInProtection = properties.getBoolean(WatchFaceService.PROPERTY_BURN_IN_PROTECTION, false)

            // Updates complications to properly render in ambient mode based on the
            // screen's capabilities.
            var complicationDrawable: ComplicationDrawable?

            for (i in complicationIds.indices) {
                complicationDrawable = mComplicationDrawableSparseArray?.get(complicationIds[i])

                if (complicationDrawable != null) {
                    complicationDrawable.setLowBitAmbient(mLowBitAmbient)
                    complicationDrawable.setBurnInProtection(mBurnInProtection)
                }

            }
        }

        //onComplicationDataUpdate()
        override fun onComplicationDataUpdate(
                complicationId: Int, complicationData: ComplicationData?) {
            Log.d(TAG, "onComplicationDataUpdate() id: $complicationId")

            // Adds/updates active complication data in the array.
            mActiveComplicationDataSparseArray?.put(complicationId, complicationData)

            // Updates correct ComplicationDrawable with updated data.
            val complicationDrawable = mComplicationDrawableSparseArray?.get(complicationId)
            complicationDrawable?.setComplicationData(complicationData)

            invalidate()
        }


        override fun onTapCommand(tapType: Int, x: Int, y: Int, eventTime: Long) {
            Log.d(TAG, "OnTapCommand()")

            //OnTapCommand()
            when (tapType) {
                WatchFaceService.TAP_TYPE_TAP -> {
                    val tappedComplicationId = getTappedComplicationId(x, y)
                    if (tappedComplicationId != -1) {
                        onComplicationTap(tappedComplicationId)
                    }
                }
            }
        }

        /*
         * Determines if tap inside a complication area or returns -1.
         */
        private fun getTappedComplicationId(x: Int, y: Int): Int {

            var complicationId: Int
            var complicationData: ComplicationData?
            var complicationDrawable: ComplicationDrawable

            val currentTimeMillis = System.currentTimeMillis()

            for (i in complicationIds.indices) {
                complicationId = complicationIds[i]
                complicationData = mActiveComplicationDataSparseArray?.get(complicationId)

                if (complicationData != null
                        && complicationData.isActive(currentTimeMillis)
                        && complicationData.type != ComplicationData.TYPE_NOT_CONFIGURED
                        && complicationData.type != ComplicationData.TYPE_EMPTY) {

                    complicationDrawable = mComplicationDrawableSparseArray!!.get(complicationId)
                    val complicationBoundingRect = complicationDrawable.bounds

                    if (complicationBoundingRect.width() > 0) {
                        if (complicationBoundingRect.contains(x, y)) {
                            return complicationId
                        }
                    } else {
                        Log.e(TAG, "Not a recognized complication id.")
                    }
                }
            }
            return -1
        }

        // Fires PendingIntent associated with complication (if it has one).
        private fun onComplicationTap(complicationId: Int) {
            // TODO: Step 5, onComplicationTap()
            Log.d(TAG, "onComplicationTap()")

            val complicationData = mActiveComplicationDataSparseArray?.get(complicationId)

            if (complicationData != null) {

                if (complicationData.tapAction != null) {
                    try {
                        complicationData.tapAction.send()
                    } catch (e: PendingIntent.CanceledException) {
                        Log.e(TAG, "onComplicationTap() tap action error: $e")
                    }

                } else if (complicationData.type == ComplicationData.TYPE_NO_PERMISSION) {

                    // Watch face does not have permission to receive complication data, so launch
                    // permission request.
                    val componentName = ComponentName(
                            applicationContext,
                            WatchfaceService::class.java)

                    val permissionRequestIntent = ComplicationHelperActivity.createPermissionRequestHelperIntent(
                            applicationContext, componentName)

                    startActivity(permissionRequestIntent)
                }

            } else {
                Log.d(TAG, "No PendingIntent for complication $complicationId.")
            }
        }

        override fun onTimeTick() {
            super.onTimeTick()
            invalidate()
        }

        override fun onAmbientModeChanged(inAmbientMode: Boolean) {
            super.onAmbientModeChanged(inAmbientMode)

            mAmbient = inAmbientMode

            updateWatchHandStyles()

            //ambient
            var complicationDrawable: ComplicationDrawable

            for (i in complicationIds.indices) {
                complicationDrawable = mComplicationDrawableSparseArray!!.get(complicationIds[i])
                complicationDrawable.setInAmbientMode(mAmbient)
            }

            // Check and trigger whether or not timer should be running (only in active mode).
            updateTimer()
        }

        //Using the elvis operator by combining it with the apply extension to set values
        private fun updateWatchHandStyles() {

            if (mAmbient) {
                mHourMinuteTicksHandPaint.apply {
                    isAntiAlias = false
                    clearShadowLayer()
                }

                mSecondHandPaint.apply {
                    isAntiAlias = false
                    clearShadowLayer()
                }
            } else {

                mHourMinuteTicksHandPaint.apply {
                    isAntiAlias = true
                    setShadowLayer(SHADOW_RADIUS.toFloat(), 0f, 0f, Color.BLACK)
                }

                mSecondHandPaint.apply {
                    isAntiAlias = true
                    setShadowLayer(SHADOW_RADIUS.toFloat(), 0f, 0f, Color.BLACK)
                }
            }
        }

        override fun onSurfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
            super.onSurfaceChanged(holder, format, width, height)

            /*
             * Find the coordinates of the center point on the screen.
             * Ignore the window insets so that, on round watches
             * with a "chin", the watch face is centered on the entire screen,
             * not just the usable portion.
             */
            mCenterX = width / 2f
            mCenterY = height / 2f

            /*
             * Calculate lengths of different hands based on watch screen size.
             */
            mSecondHandLength = (mCenterX * 0.875).toFloat()
            mMinuteHandLength = (mCenterX * 0.75).toFloat()
            mHourHandLength = (mCenterX * 0.5).toFloat()

            /*
             * Calculates location bounds for left, right, top and bottom circular complications.
             * Using at least 1/4 of the screen width for circular complications better readability.
             */

            // For most Wear devices, width and height are the same, so we just chose one (width).
            // calculating ComplicationDrawable locations
            val sizeOfComplication = width / 4
            val midpointOfScreen = width / 2

            val horizontalOffset = (midpointOfScreen - sizeOfComplication) / 2
            val verticalOffset = midpointOfScreen - sizeOfComplication / 2

            val leftBounds =
            // Left, Top, Right, Bottom
                    Rect(
                            horizontalOffset,
                            verticalOffset,
                            horizontalOffset + sizeOfComplication,
                            verticalOffset + sizeOfComplication)

            val leftComplicationDrawable = mComplicationDrawableSparseArray?.get(LEFT_COMPLICATION_ID)
            leftComplicationDrawable?.bounds = leftBounds

            val rightBounds =
            // Left, Top, Right, Bottom
                    Rect(
                            midpointOfScreen + horizontalOffset,
                            verticalOffset,
                            midpointOfScreen + horizontalOffset + sizeOfComplication,
                            verticalOffset + sizeOfComplication)

            val rightComplicationDrawable = mComplicationDrawableSparseArray?.get(RIGHT_COMPLICATION_ID)
            rightComplicationDrawable?.bounds = rightBounds

            val bottomBounds =
            // Left, Top, Right, Bottom
                    Rect(
                            midpointOfScreen - sizeOfComplication / 2,
                            height - horizontalOffset - sizeOfComplication,
                            midpointOfScreen + sizeOfComplication / 2,
                            height - horizontalOffset)

            val bottomComplicationDrawable = mComplicationDrawableSparseArray?.get(BOTTOM_COMPLICATION_ID)
            bottomComplicationDrawable?.bounds = bottomBounds


            val topBounds =
                    // Left, Top, Right, Bottom
                    Rect(
                            midpointOfScreen - sizeOfComplication / 2,
                            horizontalOffset,
                            midpointOfScreen + sizeOfComplication / 2,
                            horizontalOffset + sizeOfComplication)

            val topComplicationDrawable = mComplicationDrawableSparseArray?.get(TOP_COMPLICATION_ID)
            topComplicationDrawable?.bounds = topBounds
        }

        override fun onDraw(canvas: Canvas?, bounds: Rect?) {
            val now = System.currentTimeMillis()
            mCalendar?.timeInMillis = now

            drawBackground(canvas)

            drawComplications(canvas, now)

            drawHands(canvas)
        }

        private fun drawComplications(canvas: Canvas?, currentTimeMillis: Long) {
            var complicationId: Int
            var complicationDrawable: ComplicationDrawable?

            for (i in complicationIds.indices) {
                complicationId = complicationIds[i]
                complicationDrawable = mComplicationDrawableSparseArray?.get(complicationId)

                complicationDrawable?.draw(canvas, currentTimeMillis)
            }
        }

        private fun drawBackground(canvas: Canvas?) {
            if (mAmbient && (mLowBitAmbient || mBurnInProtection)) {
                canvas?.drawColor(Color.BLACK)
            } else {

                mBackgroundDrawable?.apply {
                    canvas?.let {
                        bounds = canvas.clipBounds
                        draw(canvas)
                    }

                }
            }
        }

        private fun drawHands(canvas: Canvas?) {
            /*
             * Draw ticks. Usually you will want to bake this directly into the photo, but in
             * cases where you want to allow users to select their own photos, this dynamically
             * creates them on top of the photo.
             */
            val innerTickRadius = mCenterX - 10
            val outerTickRadius = mCenterX
            for (tickIndex in 0..11) {
                val tickRot = (tickIndex.toDouble() * Math.PI * 2.0 / 12).toFloat()
                val innerX = Math.sin(tickRot.toDouble()).toFloat() * innerTickRadius
                val innerY = (-Math.cos(tickRot.toDouble())).toFloat() * innerTickRadius
                val outerX = Math.sin(tickRot.toDouble()).toFloat() * outerTickRadius
                val outerY = (-Math.cos(tickRot.toDouble())).toFloat() * outerTickRadius

                canvas?.drawLine(
                        mCenterX + innerX,
                        mCenterY + innerY,
                        mCenterX + outerX,
                        mCenterY + outerY,
                        mHourMinuteTicksHandPaint)

            }

            /*
             * These calculations reflect the rotation in degrees per unit of time, e.g.,
             * 360 / 60 = 6 and 360 / 12 = 30.
             */
            mCalendar?.let { calendar ->
                val seconds = calendar.get(Calendar.SECOND) + calendar.get(Calendar.MILLISECOND) / 1000f
                val secondsRotation = seconds * 6f

                val minutesRotation = calendar.get(Calendar.MINUTE) * 6f

                val hourHandOffset = calendar.get(Calendar.MINUTE) / 2f
                val hoursRotation = calendar.get(Calendar.HOUR) * 30 + hourHandOffset

                /*
                 * Saves the canvas state before we rotate it.
                 */
                canvas?.let {
                    it.save()

                    it.rotate(hoursRotation, mCenterX, mCenterY)
                    it.drawLine(
                            mCenterX,
                            mCenterY - CENTER_GAP_AND_CIRCLE_RADIUS,
                            mCenterX,
                            mCenterY - mHourHandLength,
                            mHourMinuteTicksHandPaint)

                    it.rotate(minutesRotation - hoursRotation, mCenterX, mCenterY)
                    it.drawLine(
                            mCenterX,
                            mCenterY - CENTER_GAP_AND_CIRCLE_RADIUS,
                            mCenterX,
                            mCenterY - mMinuteHandLength,
                            mHourMinuteTicksHandPaint)

                    /*
                     * Ensure the "seconds" hand is drawn only when we are in interactive mode.
                     * Otherwise, we only update the watch face once a minute.
                     */
                    if (!mAmbient) {
                        it.rotate(secondsRotation - minutesRotation, mCenterX, mCenterY)
                        it.drawLine(
                                mCenterX,
                                mCenterY - CENTER_GAP_AND_CIRCLE_RADIUS,
                                mCenterX,
                                mCenterY - mSecondHandLength,
                                mSecondHandPaint)
                    }
                    it.drawCircle(
                            mCenterX, mCenterY, CENTER_GAP_AND_CIRCLE_RADIUS, mHourMinuteTicksHandPaint)

                    /* Restore the canvas' original orientation. */
                    it.restore()
                }
            }
        }

        override fun onVisibilityChanged(visible: Boolean) {
            super.onVisibilityChanged(visible)

            if (visible) {
                registerReceiver()
                // Update time zone in case it changed while we weren't visible.
                mCalendar?.timeZone = TimeZone.getDefault()
                invalidate()
            } else {
                unregisterReceiver()
            }

            /*
             * Whether the timer should be running depends on whether we're visible
             * (as well as whether we're in ambient mode),
             * so we may need to start or stop the timer.
             */
            updateTimer()
        }

        private fun registerReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                return
            }
            mRegisteredTimeZoneReceiver = true
            val filter = IntentFilter(Intent.ACTION_TIMEZONE_CHANGED)
            this@WatchfaceService.registerReceiver(mTimeZoneReceiver, filter)
        }

        private fun unregisterReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                return
            }
            mRegisteredTimeZoneReceiver = false
            this@WatchfaceService.unregisterReceiver(mTimeZoneReceiver)
        }

        /**
         * Starts/stops the [.mUpdateTimeHandler] timer based on the state of the watch face.
         */
        private fun updateTimer() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME)
            if (shouldTimerBeRunning()) {
                mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME)
            }
        }

        /*
         * Returns whether the {@link #mUpdateTimeHandler} timer should be running. The timer
         * should only run when we're visible and in interactive mode.
         */
        private fun shouldTimerBeRunning(): Boolean {
            return isVisible && !isInAmbientMode
        }

    }

    companion object {

        private const val TAG = "ComplicationWatchFace"


        const val LEFT_COMPLICATION_ID = 0
        const val RIGHT_COMPLICATION_ID = 1
        const val BOTTOM_COMPLICATION_ID = 2
        const val TOP_COMPLICATION_ID = 3

        private const val MSG_UPDATE_TIME = 0

        private const val HOUR_AND_MINUTE_STROKE_WIDTH = 5f
        private const val SECOND_TICK_STROKE_WIDTH = 2f
        private const val CENTER_GAP_AND_CIRCLE_RADIUS = 4f
        private const val SHADOW_RADIUS = 6

        // Used by {@link ComplicationConfigActivity} to retrieve all complication ids.
        internal val complicationIds = intArrayOf(LEFT_COMPLICATION_ID, RIGHT_COMPLICATION_ID, BOTTOM_COMPLICATION_ID, TOP_COMPLICATION_ID)

        // Left, right, top and bottom dial supported types.
        private val COMPLICATION_SUPPORTED_TYPES = arrayOf(intArrayOf(ComplicationData.TYPE_RANGED_VALUE, ComplicationData.TYPE_ICON, ComplicationData.TYPE_SHORT_TEXT, ComplicationData.TYPE_SMALL_IMAGE),
                intArrayOf(ComplicationData.TYPE_RANGED_VALUE, ComplicationData.TYPE_ICON, ComplicationData.TYPE_SHORT_TEXT, ComplicationData.TYPE_SMALL_IMAGE),
                intArrayOf(ComplicationData.TYPE_RANGED_VALUE, ComplicationData.TYPE_ICON, ComplicationData.TYPE_SHORT_TEXT, ComplicationData.TYPE_SMALL_IMAGE),
                intArrayOf(ComplicationData.TYPE_RANGED_VALUE, ComplicationData.TYPE_ICON, ComplicationData.TYPE_SHORT_TEXT, ComplicationData.TYPE_SMALL_IMAGE)
        )

        // Used by {@link ComplicationConfigActivity} to retrieve id for complication locations and
        // to check if complication location is supported.

        //expose complication information, part 1
        @JvmStatic
        fun getComplicationId(
                complicationLocation: ComplicationConfigActivity.ComplicationLocation): Int {
            return when (complicationLocation) {
                ComplicationConfigActivity.ComplicationLocation.LEFT -> LEFT_COMPLICATION_ID
                ComplicationConfigActivity.ComplicationLocation.RIGHT -> RIGHT_COMPLICATION_ID
                ComplicationConfigActivity.ComplicationLocation.BOTTOM -> BOTTOM_COMPLICATION_ID
                ComplicationConfigActivity.ComplicationLocation.TOP -> TOP_COMPLICATION_ID
            }
        }

        // Used by {@link ComplicationConfigActivity} to retrieve complication types supported by
        // location.
        //expose complication information, part 3
        @JvmStatic
        fun getSupportedComplicationTypes(
                complicationLocation: ComplicationConfigActivity.ComplicationLocation): IntArray {
            return when (complicationLocation) {
                ComplicationConfigActivity.ComplicationLocation.LEFT -> COMPLICATION_SUPPORTED_TYPES[0]
                ComplicationConfigActivity.ComplicationLocation.RIGHT -> COMPLICATION_SUPPORTED_TYPES[1]
                ComplicationConfigActivity.ComplicationLocation.BOTTOM -> COMPLICATION_SUPPORTED_TYPES[2]
                ComplicationConfigActivity.ComplicationLocation.TOP -> COMPLICATION_SUPPORTED_TYPES[3]
            }
        }

        @JvmStatic
        fun getComplicationIds(): IntArray {
            return complicationIds
        }

        /*
     * Update rate in milliseconds for interactive mode. We update once a second to advance the
     * second hand.
     */
        private val INTERACTIVE_UPDATE_RATE_MS = TimeUnit.SECONDS.toMillis(1)
    }
}
